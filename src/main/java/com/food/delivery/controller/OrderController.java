 package com.food.delivery.controller;

 import com.food.delivery.dto.OStatus;
 import com.food.delivery.dto.Order;
 import com.food.delivery.dto.User;
 import com.food.delivery.service.OrderService;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.http.HttpStatus;
 import org.springframework.http.MediaType;
 import org.springframework.http.ResponseEntity;
 import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order/")
public class OrderController {

	private static final Logger logger=LoggerFactory.getLogger(OrderController.class);
	private OrderService orderService;
	
	@Autowired
	public OrderController(OrderService orderService) {
		this.orderService=orderService;
	}
	
	//Get Order
	@RequestMapping(value="/order/{orderId}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Order findByOrderId(@PathVariable("orderId") Long orderId) {
		logger.info("Finding Order by Id");
		return this.orderService.findByOrderId(orderId);
		
	}
	//Create new Order
	
	@RequestMapping(value = "/order", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Order placeOrder(@RequestBody Order order) {
		logger.info("Creating new Order");
        return this.orderService.saveOrder(order);
    }


	@RequestMapping(value = "/updateOrderStatus", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Order updateOrder(@RequestBody OStatus orderStatus) {
		logger.info("Creating new Order");
		return this.orderService.updateOrderStatus(orderStatus);
	}


	@GetMapping(value = "/orderStatus/{id}",
			consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE}
	)
	public ResponseEntity<OStatus> getOrderStatus(@PathVariable("id") Long id)  {
		logger.info("getOrderStatus() api start input userName :" + id);
		OStatus oStatus = orderService.getOrderStatus(id);
		logger.info("getUserByName() api end output user :" + id);
		return new ResponseEntity(oStatus, HttpStatus.OK);
	}
	
	//Update Order Status
	/*
	@RequestMapping(value = "/order/{orderId}", method = RequestMethod.PUT)
    public void updateOrderStatus(@PathVariable("orderId") String orderId,
                                  @RequestBody OrderStatusUpdateMessage orderStatusUpdateMessage) {
		logger.info("Updating the OrderId  " + orderId);
        this.orderService.updateOrderStatus(orderId, orderStatusUpdateMessage);
    }

	 */
	
}
