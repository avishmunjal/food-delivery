package com.food.delivery.controller;

import com.food.delivery.dto.Resturent;
import com.food.delivery.dto.User;
import com.food.delivery.service.RegistationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    RegistationService registationService;
    private Logger logger = LoggerFactory.getLogger(RegisterController.class);

    @PostMapping(value = "/user",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User> createUser(@Valid @RequestBody User createUserRequest) {
        logger.info("createUser() api start input :" + createUserRequest);
        return new ResponseEntity<User>(registationService.registerUser(createUserRequest),
                HttpStatus.OK);
    }

    @PostMapping(value = "/resturent",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Resturent> createResturent(@Valid @RequestBody Resturent createResturentRequest) {
        logger.info("createUser() api start input :" + createResturentRequest);
        return new ResponseEntity<Resturent>(registationService.registerResturent(createResturentRequest),
                HttpStatus.OK);
    }


    @GetMapping(value = "/user/{id}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<User> getUserById(@PathVariable("id") Long id)  {
        logger.info("getUserByName() api start input userName :" + id);
        User user = registationService.getUserById(id);
        logger.info("getUserByName() api end output user :" + user);
        return new ResponseEntity(user, HttpStatus.OK);
    }


    @GetMapping(value = "/resturent/{id}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<Resturent> getResturentById(@PathVariable("id") Long id)  {
        logger.info("getUserByName() api start input userName :" + id);
        Resturent resturent = registationService.getResturentById(id);
        logger.info("getResturentById() api end output user :" + resturent);
        return new ResponseEntity(resturent, HttpStatus.OK);
    }


}
