package com.food.delivery.domain;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.type.LongType;

import javax.persistence.*;

@Entity
@Table(name="Address_O")
public class Address {
/*
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Address_O.seq")
    @GenericGenerator(name = "Address.seq",
            strategy = "com.food.delivery.domain.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "USER_"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%02d")


            }

    )

 */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long addressId;
    @ManyToOne
    @JoinColumn(name="userId")
    private User user;
    @Column
    private String city;
    @Column
    private String state;
    @Column
    private String country;
    @OneToOne(cascade = {CascadeType.ALL})
    private Contact contact;

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }


}
