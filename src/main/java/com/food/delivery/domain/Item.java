package com.food.delivery.domain;

import javax.persistence.*;

@Entity
@Table(name="Item_O")
public class Item {
  /*  @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Item_O.seq")
    @GenericGenerator(name = "Item.seq",
            strategy = "com.food.delivery.domain.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "Item_"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%03d")


            }

    )

   */
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
    private Long itemId;

    @ManyToOne(cascade = {CascadeType.ALL},fetch = FetchType.EAGER)
    @JoinColumn(name = "orderId")
    private Order order;
    @Column
    private String itemName;
    @Column
    private Double pricePerQuantity;
    @Column
    private Integer numberOfQuantity;
    @Column
    private Double totalPrice;
    @OneToOne(cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH
    })
    @JoinColumn(name="orderStatusId")
    private OStatus orderStatus;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getPricePerQuantity() {
        return pricePerQuantity;
    }

    public void setPricePerQuantity(Double pricePerQuantity) {
        this.pricePerQuantity = pricePerQuantity;
    }

    public Integer getNumberOfQuantity() {
        return numberOfQuantity;
    }

    public void setNumberOfQuantity(Integer numberOfQuantity) {
        this.numberOfQuantity = numberOfQuantity;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OStatus orderStatus) {
        this.orderStatus = orderStatus;
    }


}
