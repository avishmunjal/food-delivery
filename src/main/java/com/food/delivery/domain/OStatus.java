package com.food.delivery.domain;

import javax.persistence.*;

@Entity
public class OStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderStatusId;
    @Column
    private String statusName;

    public Long getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(Long orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

}
