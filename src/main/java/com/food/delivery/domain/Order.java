package com.food.delivery.domain;

import java.util.*;

import com.food.delivery.enums.OrderStatus;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.persistence.Id;

@Entity
@Table(name = "Order_O")
public class Order {
    /*  @Id
      @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_0.seq")
      @GenericGenerator(name = "order_O.seq",
              strategy = "com.food.delivery.domain.StringPrefixedSequenceIdGenerator",
              parameters = {
                      @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                      @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "B_"),
                      @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%05d")


              }

      )

     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderId;
    @ManyToOne(cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH
    })
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    private User user;
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "orderStatusId")
    private OStatus orderStatus;
    @Column
    private Double totalPrice;
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "resturentId")
    private Resturent resturent;

    public Resturent getResturent() {
        return resturent;
    }

    public Order setResturent(Resturent resturent) {
        this.resturent = resturent;
        return this;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public Order setItemList(List<Item> itemList) {
        this.itemList = itemList;
        return this;
    }

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Item> itemList;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

}