package com.food.delivery.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name="Resturent")
public class Resturent {
   /* @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Resturent.seq")
    @GenericGenerator(name = "Resturent.seq",
            strategy = "com.food.delivery.domain.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "Resturent_"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%03d")


            }

    )

    */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long resturentId;
    @Column
    private String name;
    @Column
    private String mobileNo;
    @Column
    private String email;
    @Column
    private String address;

    public Long getResturentId() {
        return resturentId;
    }

    public void setResturentId(Long resturentId) {
        this.resturentId = resturentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
