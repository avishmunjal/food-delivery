package com.food.delivery.dto;


public class Address {

    private Long addressId;

    private String city;

    private String state;

    private String country;

    private Contact contact;

    public String getCity() {
        return city;
    }

    public Address setCity(String city) {
        this.city = city;
        return this;
    }

    public String getState() {
        return state;
    }

    public Address setState(String state) {
        this.state = state;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Address setCountry(String country) {
        this.country = country;
        return this;
    }

    public Contact getContact() {
        return contact;
    }

    public Address setContact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public Long getAddressId() {
        return addressId;
    }

    public Address setAddressId(Long addressId) {
        this.addressId = addressId;
        return this;
    }


}
