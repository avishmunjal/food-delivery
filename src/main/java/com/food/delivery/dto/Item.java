package com.food.delivery.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.food.delivery.domain.OStatus;
import com.food.delivery.domain.Order;

public class Item {
    private Long itemId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public Order order;

    private String itemName;

    private Double pricePerQuantity;

    private Integer numberOfQuantity;

    private Double totalPrice;

    private OStatus orderStatus;

    public Long getItemId() {
        return itemId;
    }

    public Item setItemId(Long itemId) {
        this.itemId = itemId;
        return this;
    }

    public Order getOrder() {
        return order;
    }

    public Item setOrder(Order order) {
        this.order = order;
        return this;
    }

    public String getItemName() {
        return itemName;
    }

    public Item setItemName(String itemName) {
        this.itemName = itemName;
        return this;
    }

    public Double getPricePerQuantity() {
        return pricePerQuantity;
    }

    public Item setPricePerQuantity(Double pricePerQuantity) {
        this.pricePerQuantity = pricePerQuantity;
        return this;
    }

    public Integer getNumberOfQuantity() {
        return numberOfQuantity;
    }

    public Item setNumberOfQuantity(Integer numberOfQuantity) {
        this.numberOfQuantity = numberOfQuantity;
        return this;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public Item setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public OStatus getOrderStatus() {
        return orderStatus;
    }

    public Item setOrderStatus(OStatus orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

}
