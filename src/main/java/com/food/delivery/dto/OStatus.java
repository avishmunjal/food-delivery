package com.food.delivery.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.JoinColumn;

public class OStatus {
    public Long getOrderStatusId() {
        return orderStatusId;
    }

    public OStatus setOrderStatusId(Long orderStatusId) {
        this.orderStatusId = orderStatusId;
        return this;
    }

    public String getStatusName() {
        return statusName;
    }

    public OStatus setStatusName(String statusName) {
        this.statusName = statusName;
        return this;
    }

    private Long orderStatusId;
    private String statusName;

    public Long getOrderId() {
        return orderId;
    }

    public OStatus setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long orderId;

}
