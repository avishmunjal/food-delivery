package com.food.delivery.dto;


import java.util.List;

public class Order {
    private Long orderId;
    private User user;
    private OStatus orderStatus ;
    private Double totalPrice;
    private Resturent resturent;

    public List<Item> getItemList() {
        return itemList;
    }

    public Order setItemList(List<Item> itemList) {
        this.itemList = itemList;
        return this;
    }

    private List<Item> itemList;

    public Long getOrderId() {
        return orderId;
    }

    public Order setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Order setUser(User user) {
        this.user = user;
        return this;
    }

    public OStatus getOrderStatus() {
        return orderStatus;
    }

    public Order setOrderStatus(OStatus orderStatus) {
        this.orderStatus = orderStatus;
        return this;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public Order setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public Resturent getResturent() {
        return resturent;
    }

    public Order setResturent(Resturent resturent) {
        this.resturent = resturent;
        return this;
    }


}
