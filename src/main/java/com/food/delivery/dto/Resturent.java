package com.food.delivery.dto;

public class Resturent {

    private Long resturentId;

    private String name;

    private String mobileNo;

    private String email;

    private String address;

    public Long getResturentId() {
        return resturentId;
    }

    public Resturent setResturentId(Long resturentId) {
        this.resturentId = resturentId;
        return this;
    }

    public String getName() {
        return name;
    }

    public Resturent setName(String name) {
        this.name = name;
        return this;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public Resturent setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Resturent setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Resturent setAddress(String address) {
        this.address = address;
        return this;
    }

}
