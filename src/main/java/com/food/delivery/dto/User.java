package com.food.delivery.dto;


import java.util.List;

public class User {
    private Long userId;
    private String firstName;
    private String lastName;
    private List<Address> addressList;

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public User setAddressList(List<Address> addressList) {
        this.addressList = addressList;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public User setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

}
