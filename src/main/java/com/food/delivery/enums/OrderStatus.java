package com.food.delivery.enums;

public enum OrderStatus {
	WAITING,
	PREPARING,
	READY,
	DELIVERED,
	CANCELED;
}