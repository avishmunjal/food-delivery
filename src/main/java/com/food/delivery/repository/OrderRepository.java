
package com.food.delivery.repository;



import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.food.delivery.domain.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order,Long> {

	Order findByOrderId(Long orderId);

	//Order findByUserId(Long userId);
}


