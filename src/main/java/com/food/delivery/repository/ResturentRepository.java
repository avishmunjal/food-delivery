package com.food.delivery.repository;

import com.food.delivery.domain.Resturent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResturentRepository extends CrudRepository<Resturent,Long> {

}
