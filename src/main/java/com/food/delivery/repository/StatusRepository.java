package com.food.delivery.repository;

import com.food.delivery.domain.OStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository  extends CrudRepository<OStatus,Long> {
}
