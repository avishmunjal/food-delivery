
package com.food.delivery.service;

import com.food.delivery.dto.OStatus;
import com.food.delivery.dto.Order;

import java.util.List;

public interface OrderService {

    Order findByOrderId(Long orderId);



  //  List<Order> findByUserId(Long userId);

    Order saveOrder(Order order);

    com.food.delivery.dto.Order updateOrderStatus(OStatus orderStatus);

    OStatus getOrderStatus(Long orderId);

}

