package com.food.delivery.service;


import com.food.delivery.dto.Resturent;
import com.food.delivery.dto.User;
import org.springframework.stereotype.Service;

@Service
public interface RegistationService {


    public User registerUser(User user);
    public Resturent registerResturent(Resturent user);
    public User getUserById(Long userId);
    public Resturent getResturentById(Long resturentId);


}
