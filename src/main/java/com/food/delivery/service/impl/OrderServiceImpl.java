
package com.food.delivery.service.impl;

import com.food.delivery.domain.Item;
import com.food.delivery.domain.Order;
import com.food.delivery.dto.OStatus;
import com.food.delivery.repository.OrderRepository;
import com.food.delivery.repository.StatusRepository;
import com.food.delivery.repository.UserRepository;
import com.food.delivery.service.OrderService;
import com.food.delivery.util.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Override
    public com.food.delivery.dto.Order findByOrderId(Long orderId) {
        return CommonUtil.populateOrderFromEnitity(orderRepository.findByOrderId(orderId));
    }
/*
    @Override
    public List<com.food.delivery.dto.Order> findByUserId(Long userId) {
        List<Order> orderList = (List<Order>) orderRepository.findByUserId(userId);
        List<com.food.delivery.dto.Order> orderList1 = new ArrayList<>();
        for(Order order :orderList){
            orderList1.add(CommonUtil.populateOrderFromEnitity(orderRepository.findByUserId(userId)));
        }
        return orderList1;
    }


 */
    @Override
    public com.food.delivery.dto.Order saveOrder(com.food.delivery.dto.Order order) {
        Order orderEntity = CommonUtil.populateOrderEnitity(order, userRepository, statusRepository);
        return CommonUtil.populateOrderFromEnitity(orderRepository.save(orderEntity));
    }

    @Override
    public com.food.delivery.dto.Order updateOrderStatus(OStatus orderStatus) {
        Optional<com.food.delivery.domain.OStatus> oStatus1;
        try {
            oStatus1 = statusRepository.findById(orderStatus.getOrderStatusId());
        } catch (Exception e){
            oStatus1 = Optional.of(new com.food.delivery.domain.OStatus());
            oStatus1.get().setStatusName(orderStatus.getStatusName());
        }
        Order order = orderRepository.findByOrderId(orderStatus.getOrderId());
        order.setOrderStatus(oStatus1.get());

        for(Item item : order.getItemList()){
            item.setOrderStatus(oStatus1.get());
        }
        return CommonUtil.populateOrderFromEnitity(orderRepository.save(order));
    }

    @Override
    public OStatus getOrderStatus(Long orderId) {
        Order order = orderRepository.findByOrderId(orderId);
        com.food.delivery.domain.OStatus oStatus = order.getOrderStatus();
        OStatus oStatus1 = new OStatus();
        oStatus1.setOrderStatusId(oStatus.getOrderStatusId());
        oStatus1.setStatusName(oStatus.getStatusName());
        return oStatus1;
    }

/*
    @Override
    public void updateOrderStatus(String orderId, OrderStatusUpdateMessage orderStatusUpdateMessage) {
        Order order = this.orderRepository.findByOrderId(orderId);
        OrderStatus newStatus = orderStatusUpdateMessage.getOrderStatus();
        order.setStatus(newStatus);
        Date lastModifiedAt = new Date();
        order.setLastModifyTime(lastModifiedAt);
       // order.getUpdateHistory().put(newStatus, lastModifiedAt);
        this.orderRepository.save(order);
        logger.info("status of order {} has been updated successfully", orderId);
    }

 */


}

