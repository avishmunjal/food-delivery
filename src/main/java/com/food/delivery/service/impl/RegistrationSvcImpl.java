package com.food.delivery.service.impl;

import com.food.delivery.dto.Resturent;
import com.food.delivery.dto.User;
import com.food.delivery.repository.ResturentRepository;
import com.food.delivery.repository.UserRepository;
import com.food.delivery.service.RegistationService;
import com.food.delivery.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class RegistrationSvcImpl implements RegistationService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ResturentRepository resturentRepository;


    @Override
    public User registerUser(User user) {
        com.food.delivery.domain.User userEnity = CommonUtil.populateUserEnity(user);
        return CommonUtil.populateUserFromEnity(userRepository.save(userEnity));
    }

    @Override
    public Resturent registerResturent(Resturent resturent) {
        com.food.delivery.domain.Resturent  resturentEnitity= CommonUtil.populateResturentEnity(resturent);
        return CommonUtil.populateResturentFromEnity(resturentRepository.save(resturentEnitity));
    }

    @Override
    public User getUserById(Long userId) {
        Optional<com.food.delivery.domain.User> user = userRepository.findById(userId);
        return CommonUtil.populateUserFromEnity(user.get());
    }

    @Override
    public Resturent getResturentById(Long resturentId) {
        Optional<com.food.delivery.domain.Resturent> resturent = resturentRepository.findById(resturentId);
        return CommonUtil.populateResturentFromEnity(resturent.get());
    }

}
