package com.food.delivery.util;

import com.food.delivery.domain.*;
import com.food.delivery.dto.Resturent;
import com.food.delivery.dto.User;
import com.food.delivery.repository.StatusRepository;
import com.food.delivery.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CommonUtil {

    public static com.food.delivery.domain.User populateUserEnity(User user) {
        com.food.delivery.domain.User userEnitity = new com.food.delivery.domain.User();
        userEnitity.setFirstName(user.getFirstName());
        userEnitity.setLastName(user.getLastName());
        List<Address> addressEntityList = new ArrayList<>();
        for (com.food.delivery.dto.Address address : user.getAddressList()) {
            Address adr = new Address();
            adr.setCountry(address.getCountry());
            adr.setState(address.getState());
            adr.setCity(address.getCity());
            Contact contact = new Contact();
            contact.setMobileNumber(address.getContact().getMobileNumber());
            contact.setEmailId(address.getContact().getEmailId());
            contact.setAddress(adr);
            adr.setContact(contact);
            adr.setUser(userEnitity);
            addressEntityList.add(adr);
        }
        userEnitity.setAddressList(addressEntityList);
        return userEnitity;

    }

    public static User populateUserFromEnity(com.food.delivery.domain.User user) {
        User userEnitity = new User();
        userEnitity.setFirstName(user.getFirstName());
        userEnitity.setLastName(user.getLastName());
        userEnitity.setUserId(user.getUserId());
        List<com.food.delivery.dto.Address> addressEntityList = new ArrayList<>();
        for (Address address : user.getAddressList()) {
            com.food.delivery.dto.Address adr = new com.food.delivery.dto.Address();
            adr.setAddressId(address.getAddressId());
            adr.setCountry(address.getCountry());
            adr.setState(address.getState());
            adr.setCity(address.getCity());
            com.food.delivery.dto.Contact contact = new com.food.delivery.dto.Contact();
            contact.setMobileNumber(address.getContact().getMobileNumber());
            contact.setEmailId(address.getContact().getEmailId());
            adr.setContact(contact);
            addressEntityList.add(adr);
        }
        userEnitity.setAddressList(addressEntityList);
        return userEnitity;

    }

    public static com.food.delivery.domain.Resturent populateResturentEnity(Resturent resturent) {
        com.food.delivery.domain.Resturent resturentEnitity = new com.food.delivery.domain.Resturent();
        resturentEnitity.setName(resturent.getName());
        resturentEnitity.setEmail(resturent.getEmail());
        resturentEnitity.setMobileNo(resturent.getMobileNo());
        resturentEnitity.setAddress(resturent.getAddress());
        return resturentEnitity;
    }

    public static Resturent populateResturentFromEnity(com.food.delivery.domain.Resturent resturent) {
        Resturent resturentEnitity = new Resturent();
        resturentEnitity.setResturentId(resturent.getResturentId());
        resturentEnitity.setName(resturent.getName());
        resturentEnitity.setEmail(resturent.getEmail());
        resturentEnitity.setMobileNo(resturent.getMobileNo());
        resturentEnitity.setAddress(resturent.getAddress());
        return resturentEnitity;
    }


    public static Order populateOrderEnitity(com.food.delivery.dto.Order order,
                                             UserRepository userRepository, StatusRepository statusRepository) {
        Order orderEnitity = new Order();
        orderEnitity.setTotalPrice(order.getTotalPrice());
        Optional<com.food.delivery.domain.User> user = userRepository.findById(order.getUser().getUserId());
        orderEnitity.setUser(user.get());
        Optional<OStatus> oStatus = null;
        try {
            oStatus = statusRepository.findById(order.getOrderStatus().getOrderStatusId());

        } catch (Exception ee) {
            oStatus = Optional.of(new OStatus());
            oStatus.get().setStatusName(order.getOrderStatus().getStatusName());
            statusRepository.save(oStatus.get());
        }
        orderEnitity.setOrderStatus(oStatus.get());
        orderEnitity.setResturent(populateResturentEnity(order.getResturent()));
        orderEnitity.setTotalPrice(order.getTotalPrice());
        List<Item> itemList = new ArrayList<>();
        for (com.food.delivery.dto.Item item : order.getItemList()) {
            Item it = new Item();
            it.setItemName(item.getItemName());
            it.setNumberOfQuantity(item.getNumberOfQuantity());
            it.setOrderStatus(item.getOrderStatus());
            it.setPricePerQuantity(item.getPricePerQuantity());
            it.setTotalPrice(item.getTotalPrice());
            it.setOrderStatus(item.getOrderStatus());
            it.setOrder(orderEnitity);
            itemList.add(it);

        }
        orderEnitity.setItemList(itemList);
        return orderEnitity;
    }


    public static com.food.delivery.dto.Order populateOrderFromEnitity(Order order
                                             ) {
        com.food.delivery.dto.Order orderEnitity = new com.food.delivery.dto.Order();
        orderEnitity.setOrderId(order.getOrderId());
        orderEnitity.setTotalPrice(order.getTotalPrice());
        Optional<com.food.delivery.domain.User> user = Optional.ofNullable(order.getUser());
        orderEnitity.setUser(populateUserFromEnity(user.get()));
        Optional<OStatus> oStatus = Optional.ofNullable(order.getOrderStatus());
        com.food.delivery.dto.OStatus oStatus1 = new com.food.delivery.dto.OStatus();
        oStatus1.setOrderStatusId(oStatus.get().getOrderStatusId());
        oStatus1.setStatusName(oStatus.get().getStatusName());
        orderEnitity.setOrderStatus(oStatus1);
        orderEnitity.setResturent(populateResturentFromEnity(order.getResturent()));
        orderEnitity.setTotalPrice(order.getTotalPrice());
        List<com.food.delivery.dto.Item> itemList = new ArrayList<>();
        for (Item item : order.getItemList()) {
            com.food.delivery.dto.Item it = new com.food.delivery.dto.Item();
            it.setItemId(item.getItemId());
            it.setItemName(item.getItemName());
            it.setNumberOfQuantity(item.getNumberOfQuantity());
            it.setOrderStatus(item.getOrderStatus());
            it.setPricePerQuantity(item.getPricePerQuantity());
            it.setTotalPrice(item.getTotalPrice());
            it.setOrderStatus(item.getOrderStatus());
            itemList.add(it);

        }
        orderEnitity.setItemList(itemList);
        return orderEnitity;
    }

}
